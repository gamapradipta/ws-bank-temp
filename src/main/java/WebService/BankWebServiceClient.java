package WebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class BankWebServiceClient {
    public static void main(String[] args) throws MalformedURLException {
        URL url;
        url = new URL("http://localhost:1000/WebService?wsdl");

        QName qname = new QName("http://WebService/","BankWebServiceImplService");

        Service service = Service.create(url,qname);

        BankWebService eif = service.getPort(BankWebService.class);
        System.out.println(eif.getAccountInfo(12341234).getTransaction(1).getTransactionTime());

    }
}
