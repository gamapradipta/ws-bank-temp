package WebService;
import Model.*;

import javax.jws.*;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.sql.Timestamp;

@WebService(serviceName = "BankWebService")
@SOAPBinding(style = Style.RPC)
public interface BankWebService {

    @WebMethod(operationName = "validateAccNumber")
    public boolean validateAccNumber(@WebParam(name = "accNumber")int accNumber);

    @WebMethod(operationName = "transfer")
    public boolean transfer(@WebParam(name = "accNumber")int accNumber, @WebParam(name = "destNumber") int destNumber, @WebParam(name = "amount") double amount);

    @WebMethod(operationName = "makeVirtualAcc")
    public int makeVirtualAcc(@WebParam(name = "accNumber") int accNumber);

    @WebMethod(operationName = "validateVirtualNumber")
    public boolean validateVirtualNumber(@WebParam(name = "virtualNumber")int virtualNumber);

    @WebMethod(operationName = "getAccountInfo")
    public AccountInfo getAccountInfo(@WebParam(name = "accNumber")int accNumber);

    @WebMethod(operationName = "checkTransactionWithInTime")
    public boolean checkTransactionWithInTime(@WebParam(name = "destNumber")int destNumber, @WebParam(name = "amount") double amount, @WebParam(name="start") String start, @WebParam(name="end") String end );
}
