package WebService;

import javax.xml.ws.Endpoint;

public class BankWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://127.0.0.1:1000/WebService",new WebService.BankWebServiceImpl());
    }
}
