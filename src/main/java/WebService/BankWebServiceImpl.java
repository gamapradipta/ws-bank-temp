package WebService;

import javax.jws.*;
import java.sql.*;
import java.util.Random;
import Model.*;

@WebService(endpointInterface = "WebService.BankWebService")
public class BankWebServiceImpl implements BankWebService {

    private String dbUrl = "jdbc:mysql://localhost/db-bank";
    private String dbClass = "com.mysql.cj.jdbc.Driver";
    private String username = "root";

    private boolean checkEnoughBalance(double amount, int accNumber){
        String query = "SELECT balance FROM account WHERE acc_number="+accNumber;
        double balance = 0;
        boolean valid = false;
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                System.out.println(query);
                balance = rs.getDouble("balance");
            }
            if (balance>=amount){
                valid = true;
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return valid;
    }

    private int generateRandomVirtualAcc(){
        Random rnd = new Random();
        return 10000000 + rnd.nextInt(90000000);
    }

    private int getVirtualDestinationAddress(int virtualAddr){
        String query = "SELECT acc_number FROM virtual_account WHERE virtual_number="+virtualAddr;
        int destNumber = virtualAddr;
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                destNumber = rs.getInt("acc_number");
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return -999;
        }
        return destNumber;
    }

    @Override
    public boolean validateAccNumber(int accNumber) {
        boolean valid = false;
        String query = "Select * from account where acc_number =" + accNumber;
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                valid = true;
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            valid = false;
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public boolean transfer(int accNumber, int destNumber, double amount) {
        boolean valid = false;
        int virtualNumber;
        String query_debit;
        String query_credit;
        if (validateAccNumber(accNumber) && (validateAccNumber(destNumber) || validateVirtualNumber(destNumber))){
            if (checkEnoughBalance(amount,accNumber)) {
                if (validateVirtualNumber(destNumber)) {
                    virtualNumber =destNumber;
                    destNumber = getVirtualDestinationAddress(destNumber);
                    if (destNumber == -999){
                        return false;
                    }
                    query_debit = "INSERT INTO transaction (acc_number,related_number,amount,type,time_transaction,virtual_number) VALUE (" + accNumber + "," + destNumber + "," + amount + ",'DEBIT',NOW(),"+virtualNumber+")";
                    query_credit = "INSERT INTO transaction (acc_number,related_number,amount,type,time_transaction,virtual_number) VALUE (" + destNumber + "," + accNumber + "," + amount + ",'KREDIT',NOW(),"+virtualNumber+")";
                }
                else{
                    query_debit = "INSERT INTO transaction (acc_number,related_number,amount,type,time_transaction) VALUE (" + accNumber + "," + destNumber + "," + amount + ",'DEBIT',NOW())";
                    query_credit = "INSERT INTO transaction (acc_number,related_number,amount,type,time_transaction) VALUE (" + destNumber + "," + accNumber + "," + amount + ",'KREDIT',NOW())";
                }
                String query_balance_debit = "UPDATE account SET balance = balance - " + amount + " WHERE acc_number = " + accNumber;
                String query_balance_credit = "UPDATE account SET balance = balance + " + amount + " WHERE acc_number = " + destNumber;
                try {
                    Class.forName(dbClass);
                    Connection conn = DriverManager.getConnection(dbUrl, username,null);
                    Statement stmt = conn.createStatement();
                    stmt.execute(query_debit);
                    stmt.execute(query_credit);
                    stmt.execute(query_balance_debit);
                    stmt.execute(query_balance_credit);
                    conn.close();
                    valid = true;
                } catch (ClassNotFoundException | SQLException e) {
                    valid = false;
                    e.printStackTrace();
                }
            }
        }
        return valid;
    }

    @Override
    public int makeVirtualAcc(int accNumber) {
        int virtualNumber = generateRandomVirtualAcc();
        //If the virtualNumber exist in database, generate random int again
        while (validateAccNumber(virtualNumber) || validateAccNumber(virtualNumber) ){
            virtualNumber = generateRandomVirtualAcc();
        }
        System.out.println(virtualNumber);
        String query = "INSERT INTO virtual_account(virtual_number,acc_number) VALUE ("+virtualNumber+","+accNumber+")";
        System.out.println(query);
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            stmt.execute(query);
            conn.close();
        } catch (ClassNotFoundException e) {
            //Error
            virtualNumber = -999;
            e.printStackTrace();
        }catch (SQLException e){
            virtualNumber = -999;
            e.printStackTrace();
        }
        return virtualNumber;
    }

    @Override
    public boolean validateVirtualNumber(int virtualNumber) {
        boolean valid = false;
        String query = "Select * from virtual_account where virtual_number =" + virtualNumber;
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()){
                valid = true;
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            valid = false;
            e.printStackTrace();
        }
        return  valid;
    }

    @Override
    public AccountInfo getAccountInfo(int accNumber) {
        AccountInfo accountInfo = new AccountInfo();
        String query_getAccInfo  = "SELECT name, acc_number, balance FROM account WHERE acc_number="+accNumber;
        String query_getCountTransaction = "Select count(*) as countTransaction FROM transaction WHERE acc_number="+accNumber;
        String query_getTransaction = "Select related_number , amount, type, time_transaction FROM transaction WHERE acc_number="+accNumber;
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet raw_getAccInfo = stmt.executeQuery(query_getAccInfo);
            System.out.println("masuk");
            System.out.println(query_getTransaction);
            System.out.println("lagi");
            String name="Empty";
            int acc_number=-999,count=0;
            double balance=-999;
            while (raw_getAccInfo.next()){
                System.out.println("name");
                name = raw_getAccInfo.getString("name");
                System.out.println("acc_number");
                acc_number=raw_getAccInfo.getInt("acc_number");
                System.out.println("balance");
                balance=raw_getAccInfo.getDouble("balance");
            }
            ResultSet raw_getCountTransaction = stmt.executeQuery(query_getCountTransaction);
            if (raw_getCountTransaction.next()){
                count = raw_getCountTransaction.getInt("countTransaction");
            }
            ResultSet raw_getTransaction = stmt.executeQuery(query_getTransaction);
            accountInfo = new AccountInfo(name,acc_number,balance,count);
            int i = 0;
            while(raw_getTransaction.next()){
                Transaction temp = new Transaction();
                temp.setDestAccNumber(raw_getTransaction.getInt("related_number"));
                temp.setType(raw_getTransaction.getString("type"));
                temp.setAmount(raw_getTransaction.getDouble("amount"));
                temp.setTransactionTime(raw_getTransaction.getTimestamp("time_transaction"));
                accountInfo.setTransaction(i,temp);
                i++;
            }
        } catch (ClassNotFoundException e) {
            System.out.println("gagal");
//            e.printStackTrace();
        }catch (SQLException e){
            System.out.println("gagal lagi");
        }
        return accountInfo;
    }

    @Override
    public boolean checkTransactionWithInTime(int destNumber, double amount, String start, String end) {
        boolean valid = false;
        String query = "SELECT * FROM transaction WHERE type like 'KREDIT' AND amount = "+amount+" AND (acc_number = " +destNumber+" OR   virtual_number = "+destNumber+") AND ( time_transaction BETWEEN '"+start+"' AND '"+end+"' )";
        System.out.println(query);
        try{
            Class.forName(dbClass);
            Connection conn = DriverManager.getConnection(dbUrl,username,null);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                valid = true;
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            valid = false;
            e.printStackTrace();
        }
        return valid;
    }

}
