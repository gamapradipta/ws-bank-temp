package Model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.XmlType;
import java.util.Collection;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Record")
public class AccountInfo {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "accNumber")
    private int accNumber;
    @XmlElement(name = "balance")
    private double balance;
    @XmlElement(name = "transactionHistory")
    private Transaction[] transactionHistory;

    public AccountInfo(){
        this.name = "Empty";
        this.accNumber = -999;
        this.balance = -999;
        this.transactionHistory = new Transaction[1];
        this.transactionHistory[0] = new Transaction();
    }

    public AccountInfo(String name, int accNumber, Double balance, int count){
        this.name = name;
        this.accNumber = accNumber;
        this.balance = balance;
        this.transactionHistory = new Transaction[count];
        for (int i=0;i<count;i++){
            this.transactionHistory[i] = new Transaction();
        }
    }

    public Transaction[] getTransactionHistory() {
        return transactionHistory;
    }

    public void setTransactionHistory(Transaction[] transactionHistory) {
        this.transactionHistory = transactionHistory;
    }

    public void setTransaction(int i, Transaction transaction){
        this.transactionHistory[i] = transaction;
    }

    public Transaction getTransaction(int i){
        return this.transactionHistory[i];
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(int accNumber) {
        this.accNumber = accNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
