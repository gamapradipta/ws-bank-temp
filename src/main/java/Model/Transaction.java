package Model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.XmlType;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name ="TransactionList")
public class Transaction {
    @XmlElement(name = "destAccNumber", required = true)
    private int destAccNumber;
    @XmlElement(name = "type", required = true)
    private String type;
    @XmlElement(name = "amount", required = true)
    private double amount;
    @XmlElement(name = "transactionTime", required = true)
    private String transactionTime;

    public Transaction(){
        this.destAccNumber = -999;
        this.type = "Empty";
        this.amount = -999;
        this.transactionTime = "Empty";
    }
    public Transaction(int destAccNumber, String type, Double amount, String transactionTime){
        this.destAccNumber = destAccNumber;
        this.type = type;
        this.amount = amount;
        this.transactionTime = transactionTime;
    }
    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Timestamp transactionTime) {
        this.transactionTime = transactionTime.toString();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDestAccNumber() {
        return destAccNumber;
    }

    public void setDestAccNumber(int destAccNumber) {
        this.destAccNumber = destAccNumber;
    }
}
